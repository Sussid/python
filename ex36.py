from sys import exit

#global variables:
inventory = ["Fists"]
hasHammer = False
hasPoison = False
hasOpenedChest = False
hasCrossed=False


#useful input, etc. handlers:
def GetPosInt():
	while True:
		try:
			intTarget = int(raw_input("Enter value: "))
		except ValueError:
			print "Try again."
			continue
		else:
			if intTarget < 0:
				print "Try again."
				continue
			else:
				return (intTarget)



def hammaroom():
	global inventory
	global hasHammer
	
	print "\nYou explore a bit further into the side corridor"

	while True:
		if "Hammer" in inventory:
			print "The room is quite empty now, without the hammer you've taken."
			print "\nThe exits are (S)outh"
		else:
			print "You find yourself in a room with blah and a hammer in the corner."
			print "\nYou can (Take) the (Hammer). Or you can exit (S)outh."
		
		choice = raw_input("")
		if "ake" in choice and "ammer" in choice and not hasHammer:
			hasHammer=True
			inventory.append("Hammer")
		elif "ake" in choice and "ammer" in choice and hasHammer:
			print "You've already taken the hammer"
		
		elif choice == "S":
			crossRoads1()
		else:
			continue

		
def poisonRoom():
	global inventory
	global hasPoison
	
	print "\nYou follow the narrow to passage to find yourself in a small room"
	
	while True:
		if "Poison" in inventory:
			print "\nThere's not much else to do here, now that you have the Poison."
			print "\nThe only exit is to the (W)est."
		else:
			print "\nThere's a bottle of green liquid on tiny cupboard. It looks like (Poison)."
			print "You probably should (Take) it with you."
			
		choice = raw_input("")
		if "ake" in choice and "oison" in choice and not hasPoison:
			hasPoison = True
			inventory.append("Poison")
		elif "ake" in choice and "oison" in choice and hasPoison:
			print "You've already taken the Poison."
		elif choice == "W":
			crossRoads3()

def chestRoom():
	global hasOpenedChest
	
	print "\nThe door opens up to a circular room with a pedestal in the middle."
	
	while True:
		if hasOpenedChest:
			print "\nOn the pedestal there is the small chest you've already opened and caught whatever terrible there was inside it."
		elif not hasOpenedChest:
			print "\nOn the pedestal there is a closed (Chest). You could try to (Open) it or"
			print "you can leave to room to (N)orth."
		print "\nWhat are you going to do?"
		choice = raw_input("")
		if "pen" in choice and "hest" in choice and not hasOpenedChest:
			hasOpenedChest = True
			print "\nNow you've done it. You hear a terrible scream and feel nauseous."
		elif "pen" in choice and "hest" in choice and hasOpenedChest:
			print "\nThere's nothing to open anymore."
		elif choice == "N":
			crossRoads3()

		

def lavaRoom():
	
	global hasCrossed
	
	print "You enter a vast room that is divided by a river of molten lava."
		
	while True:
		if not hasCrossed:
			print "\nThere's a door to the (N)orth, but first you must cross the molten river."
			print "You can also fuck off towards the (S)outh in case you forgot something."
			print "\nWhen looking around a bit, you notice a couple of ways to cross the burning gap."
			print "There's a very shoddy looking (Bridge) you can attempt to cross."
			print "There is a (Rope) hanging from the ceiling that's fastened under stone on your side of the lava flow."
			print "Also, a (Boat) sits on this bank that might be of use to you."
		elif hasCrossed:
			print "Now that you've mastered the rope, you can move over the lava river freely."
			print "You can go (N)orth or (S)outh."
			
		choice = raw_input ("")
		if "Rope" in choice or "rope" in choice:
			hasCrossed=True
			print "You use the rope to swing across. A regular tarzan, you."
		elif "boat" in choice or "Boat" in choice:
			dead("The boat is flammable,")
		elif "bridge" in choice or "Bridge" in choice:
			dead("The bridge gives way, and you fall into the lava,")
		elif hasCrossed and choice == "N":
			bossRoom()		
		elif hasCrossed and choice == "S":
			crossRoads2()
		else:
			print "como?"

def bossRoom():
	global inventory
	global hasHammer
	global hasPoison
	global hasOpenedChest

	# bossCounter = 0
	
	print "You've arrived at the room filled with the smell most foul."
	print "There's something lurking and growling about in the shadows."
	print "Before you can think twice, there's a towering monstrosity looming over you, poised to strike."
	print "What will you do?"
	
	if hasOpenedChest:
		if hasHammer and hasPoison:
			bossSelections(3, inventory)
		else:
			print "To say you've come underprepped is an understatement."
			print "The monster immediately takes a swing at you."
			dead("You've been smeared all over the room")	
	elif not hasOpenedChest:
		if hasHammer or hasPoison:
			bossSelections(2, inventory)
		else:
			print "To say you've come underprepped is an understatement."
			print "The monster immediately takes a swing at you."
			dead("You've been smeared all over the room")
	else:
		dead("Code's broken.")
		

def crossRoads1():
	while True:
		print "(N)orth or (E)ast"
		choice = raw_input("> ")
		if choice == "N":
			hammaroom()
		elif choice == "E":
			crossRoads2()
		else:
			print "I have no idea. Please input the following:"

def crossRoads2():
	print "You follow along the corridor.\n"
	print "There sidepassage going toward (N)orth and it emanates warmth and there's a light glow coming further down."
	print "There's also a possibilty to follow the corridor to (E)astward."
	print "Or go back toward (W)est."
	while True:
		print "\nPick your direction:"
		choice = raw_input("> ")
		if choice == "N":
			lavaRoom()
		elif choice == "W":
			crossRoads1()
		elif choice == "E":
			crossRoads3()
		else:
			print "\nI don't know what that means."
			print "Try again."

def crossRoads3():
	print "You follow along the corridor.\n"
	print "There's a narrow passage that continues to the (E)ast."
	print "You can also see a door to your direct (S)outh."
	print "You can also return to (W)est."
	print "Where to?"
	while True:
		choice = raw_input("> ")
		if choice == "E":
			poisonRoom()
		elif choice == "S":
			chestRoom()
		elif choice == "W":
			crossRoads2()
		else:
			print "\nWhat?"

def bossSelections(nombre, inventory):
	bossCounter = 0
	while bossCounter < nombre:
		print "\nYou have the following options:\n"
		for i in inventory:
			print "%s" %i
		choice = raw_input("\n> ")
		if "ammer" in choice:
			bossCounter = bossCounter + 1
			print "You strike the boss with the Hammer."
			print "The boss bleeds, but the hammer gets stuck."
			inventory.pop(inventory.index("Hammer"))
		elif "oison" in choice:
			bossCounter = bossCounter + 1
			print "You throw the Poison vial at the monster."
			print "It burns the monsters skin badly."
			inventory.pop(inventory.index("Poison"))
		elif "ists" in choice:
			bossCounter = bossCounter + 1
			print "You smack the boss with your bare fists"
			print "It staggers."
			inventory.pop(inventory.index("Fists"))
		else:
			print "Wuh?"
	print "That last move was the last drop. The monster groans."
	print "You gosh darn killed it. Congratulations!"
	exit (0)
			
def dead(why):
	print why, "Drumbass"
	exit(0)

	
def start():
	print "Hey!"
	print "How many legs does a spider have?"
	i = 0
	while i < 3:
		choice = int(raw_input("> "))
		if choice == 6:
			crossRoads1()
		else:
			print ("Wrong. Try Again!")
			i+= 1
	dead("\nToo many false tries.")
			
start()
